﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork
{
    /*
     * Helper class for loading all those images
     */

    class DataLoader
    {
        private BinaryReader trainingLabels;
        private BinaryReader trainingImages;

        private BinaryReader testLabels;
        private BinaryReader testImages;

        public DataLoader()
        {
            trainingLabels = new BinaryReader(new FileStream("..//..//data/train-labels.idx1-ubyte", FileMode.Open));
            trainingImages = new BinaryReader(new FileStream("..//..//data/train-images.idx3-ubyte", FileMode.Open));

            testLabels = new BinaryReader(new FileStream("..//..//data/t10k-labels.idx1-ubyte", FileMode.Open));
            testImages = new BinaryReader(new FileStream("..//..//data/t10k-images.idx3-ubyte", FileMode.Open));
        }

        public byte[][] LoadTrainingImages()
        {
            byte[][] imageData = new byte[28][];

            trainingImages.ReadInt32();
            int 

            return imageData;
        }
    }
}
