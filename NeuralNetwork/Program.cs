﻿using System;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork
{
    class Program
    {
        static int Main(string[] args)
        {
            if (args.Length != 1) {
                Console.WriteLine("usage: <image>");
                return 1;
            }

            Bitmap img = new Bitmap(args[0]);

            int inputNeuronLen = img.Width * img.Height;
            float[] pixels = new float[inputNeuronLen];

            for(int i = 0; i < inputNeuronLen; i++)
            {
                pixels[i] = ((float)(img.GetPixel(i % 28, i / 28).ToArgb() & 0x00FFFFFF) / 0xFFFFFF);
                //Console.WriteLine(pixels[i]);
            }

            Network n = new Network(inputNeuronLen, 16, 10);

            Console.WriteLine("Network chose {0}", n.Categorize(pixels));
            return 0;
        }
    }
}
