﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetwork
{
    class Network
    {
        int numLayers;
        int[] sizes;
        float[][] biases;
        float[][][] weights;

        Random RandomSource;

        public Network(params int[] sizes)
        {
            numLayers = sizes.Length;
            this.sizes = sizes;
            biases = new float[numLayers - 1][];
            weights = new float[numLayers][][];

            this.RandomSource = new Random();

            InitializeBiases();
            InitializeWeights();

        }

        private void InitializeBiases()
        {
            //set biases (input layer has no biases)
            for(int i = 0; i < numLayers - 1; i++)
            {
                biases[i] = new float[sizes[i + 1]];
                for(int j = 0; j < sizes[i + 1]; j++)
                {
                    biases[i][j] = this.InitialBias();
                }
            }
        }

        private void InitializeWeights()
        {
            //for each layer after the first (input) layer, assign random
            //weights for the inputs of each perceptron in that layer
            for(int i = 0; i < numLayers - 1; i++)
            {
                weights[i] = new float[sizes[i + 1]][];
                for(int j = 0; j < weights[i].Length; j++)
                {
                    weights[i][j] = new float[sizes[i]];
                    for(int k = 0; k < weights[i][j].Length; k++)
                    {
                        weights[i][j][k] = InitialBias();
                    }
                }
            }
        }

        /**
         *
         */
        private float InitialBias()
        {
            return (float)(RandomSource.NextDouble() * 6 - 3);
        }

        //Squish things with this
        private float Sigmoid(float z)
        {
            return (1.0f / (1.0f + (float)Math.Exp(-z)));
        }

        //This is where the loads of input becomes some output
        public float[] FeedForward(float[] a)
        {
            //outputs, or new inputs to the next layer in the network
            float[] aPrime;

            for(int i = 0; i < numLayers - 1; i++)
            {
                aPrime = new float[sizes[i + 1]];
                for(int j = 0; j < weights[i].Length; j++)
                {
                    float sum = 0.0f;
                    for(int k = 0; k < weights[i][j].Length; k++)
                    {
                        sum += a[k] * weights[i][j][k];
                    }

                    aPrime[j] = Sigmoid(sum + biases[i][j]);
                }

                a = aPrime;
            }
            return a;
        }

        public int Categorize(float[] inputs)
        {
            float[] result = FeedForward(inputs);

            // The one the network chose is the neuron with the
            // highest score.
            int choice = -1;
            float best = Single.NegativeInfinity; // obviously wrong answer :]
            for (int i = 0; i < result.Length; i++) {
                if (result[i] > best) {
                    choice = i;
                    best = result[i];
                }
            }

            return choice;
        }

        public void Train(float[] inputs, int expectedOutput)
        {
        }
    }
}
